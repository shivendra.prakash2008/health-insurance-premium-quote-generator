package com.emid.healthInsurance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.HttpServletBean;

import com.emid.healthInsurance.bean.HealthInsuranceBean;
import com.emid.healthInsurance.service.IHealthInsurance;

@Controller
public class HealthInsuranceController {

	@Autowired
	IHealthInsurance healthInsurance;

	@RequestMapping(value = "/checkPremium", method = RequestMethod.POST)
	@ResponseBody
	public String checkPremium(@RequestBody HealthInsuranceBean healthInsuranceBean, HttpServletBean request) {
		return healthInsurance.checkPremium(healthInsuranceBean);

	}
}