/**
 * 
 */
package com.emid.healthInsurance.service;

import com.emid.healthInsurance.bean.HealthInsuranceBean;

/**
 * @author shivendra
 *
 */
public interface IHealthInsurance {

	String checkPremium(HealthInsuranceBean healthInsuranceBean);

}
