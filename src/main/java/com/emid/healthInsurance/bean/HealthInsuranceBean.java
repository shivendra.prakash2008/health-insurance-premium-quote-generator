/**
 * 
 */
package com.emid.healthInsurance.bean;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author shivendra
 *
 */
public class HealthInsuranceBean {
	
	@JsonProperty("Name")
	private String name;
	
	@JsonProperty("Gender")
	private String gender;
	
	@JsonProperty("Age")
	private String age;
	
	@JsonProperty("Current health")
	private CurrentHealthBean currentHealth;
	
	@JsonProperty("Habits")
	private HabitsBean habits;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender
	 *            the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the age
	 */
	public String getAge() {
		return age;
	}

	/**
	 * @param age
	 *            the age to set
	 */
	public void setAge(String age) {
		this.age = age;
	}

	/**
	 * @return the currentHealth
	 */
	public CurrentHealthBean getCurrentHealth() {
		return currentHealth;
	}

	/**
	 * @param currentHealth
	 *            the currentHealth to set
	 */
	public void setCurrentHealth(CurrentHealthBean currentHealth) {
		this.currentHealth = currentHealth;
	}

	/**
	 * @return the habits
	 */
	public HabitsBean getHabits() {
		return habits;
	}

	/**
	 * @param habits
	 *            the habits to set
	 */
	public void setHabits(HabitsBean habits) {
		this.habits = habits;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "HealthInsuranceBean [name=" + name + ", gender=" + gender + ", age=" + age + ", currentHealth="
				+ currentHealth + ", habits=" + habits + "]";
	}

}
