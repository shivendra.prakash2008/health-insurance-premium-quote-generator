/**
 * 
 */
package com.emid.healthInsurance.bean;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author shivendra
 *
 */
public class CurrentHealthBean {

	@JsonProperty("Hypertension")
	private String hyperTension;
	
	@JsonProperty("Blood pressure")
	private String bloodPressure;
	
	@JsonProperty("Blood sugar")
	private String bloodSugar;
	
	@JsonProperty("Overweight")
	private String overWeight;

	/**
	 * @return the hyperTension
	 */
	public String getHyperTension() {
		return hyperTension;
	}

	/**
	 * @param hyperTension
	 *            the hyperTension to set
	 */
	public void setHyperTension(String hyperTension) {
		this.hyperTension = hyperTension;
	}

	/**
	 * @return the bloodPressure
	 */
	public String getBloodPressure() {
		return bloodPressure;
	}

	/**
	 * @param bloodPressure
	 *            the bloodPressure to set
	 */
	public void setBloodPressure(String bloodPressure) {
		this.bloodPressure = bloodPressure;
	}

	/**
	 * @return the bloodSugar
	 */
	public String getBloodSugar() {
		return bloodSugar;
	}

	/**
	 * @param bloodSugar
	 *            the bloodSugar to set
	 */
	public void setBloodSugar(String bloodSugar) {
		this.bloodSugar = bloodSugar;
	}

	/**
	 * @return the overWeight
	 */
	public String getOverWeight() {
		return overWeight;
	}

	/**
	 * @param overWeight
	 *            the overWeight to set
	 */
	public void setOverWeight(String overWeight) {
		this.overWeight = overWeight;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CurrentHealthBean [hyperTension=" + hyperTension + ", bloodPressure=" + bloodPressure + ", bloodSugar="
				+ bloodSugar + ", overWeight=" + overWeight + "]";
	}

}
