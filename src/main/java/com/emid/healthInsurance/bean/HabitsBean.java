/**
 * 
 */
package com.emid.healthInsurance.bean;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author shivendra
 *
 */
public class HabitsBean {
	
	@JsonProperty("Smoking")
	private String smoking;
	
	@JsonProperty("Alcohol")
	private String alcohol;
	
	@JsonProperty("Daily exercise")
	private String dailyExercise;
	
	@JsonProperty("Drugs")
	private String drugs;
	/**
	 * @return the smoking
	 */
	public String getSmoking() {
		return smoking;
	}
	/**
	 * @param smoking the smoking to set
	 */
	public void setSmoking(String smoking) {
		this.smoking = smoking;
	}
	/**
	 * @return the alcohol
	 */
	public String getAlcohol() {
		return alcohol;
	}
	/**
	 * @param alcohol the alcohol to set
	 */
	public void setAlcohol(String alcohol) {
		this.alcohol = alcohol;
	}
	/**
	 * @return the dailyExercise
	 */
	public String getDailyExercise() {
		return dailyExercise;
	}
	/**
	 * @param dailyExercise the dailyExercise to set
	 */
	public void setDailyExercise(String dailyExercise) {
		this.dailyExercise = dailyExercise;
	}
	/**
	 * @return the drugs
	 */
	public String getDrugs() {
		return drugs;
	}
	/**
	 * @param drugs the drugs to set
	 */
	public void setDrugs(String drugs) {
		this.drugs = drugs;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "HabitsBean [smoking=" + smoking + ", alcohol=" + alcohol + ", dailyExercise=" + dailyExercise
				+ ", drugs=" + drugs + "]";
	}

	

}
